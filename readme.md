Manual

1 Create database

Open JvZijp.TvMaze.Storage.SqlImpl.TvMazeContext
Update the connection string in method OnConfiguring
Open Nuget Package Manager Console
Navigate to JvZijp.TvMaze.Storage.SqlImpl project
run command: dotnet ef database update

2 Run scraper
Either run from command line: dotnet JvZijp.TvMaze.Scraper.dll 
Or run from code

3 RestApi
Run JvZijp.TvMaze.RestApi from code. 
Default launchUrl is /api/shows/page/0
Where the number is the page number