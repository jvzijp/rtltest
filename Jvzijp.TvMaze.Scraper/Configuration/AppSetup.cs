﻿using System;
using System.IO;
using JvZijp.TvMaze.Scraper.Scraping;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace JvZijp.TvMaze.Scraper.Configuration
{
    class AppSetup
    {
        public static IServiceCollection SetUp()
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (string.IsNullOrWhiteSpace(environment))
            {
                environment = "Development";
            }

            // Create a service collection and configure our dependencies
            var serviceCollection = new ServiceCollection();

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("Configuration/JvZijp.TvMaze.Scraper.Settings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"Configuration/AppConfig/Logging.settings.{environment}.json", optional: true, reloadOnChange: true);

            IConfiguration config = builder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            serviceCollection.AddLogging();
            serviceCollection.AddSingleton<ITvMazeScraper, TvMazeScraper>();

            Domain.Configuration.ProjectSetup.ConfigureProject(serviceCollection);
            Storage.SqlImpl.Configuration.ProjectSetup.ConfigureProject(serviceCollection);
            Api.Configuration.ProjectSetup.ConfigureProject(serviceCollection);

            return serviceCollection;
        }
    }
}
