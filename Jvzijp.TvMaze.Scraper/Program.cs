﻿using System;
using System.Diagnostics;
using System.Reflection;
using JvZijp.TvMaze.Scraper.Configuration;
using JvZijp.TvMaze.Scraper.Scraping;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace JvZijp.TvMaze.Scraper
{
    class Program
    {
        static void Main(string[] args)
        {
            var totalStopWatch = new Stopwatch();
            ServiceProvider serviceProvider;

            try
            {
                //Start initialization
                var assembly = Assembly.GetExecutingAssembly();
                var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                var version = fvi.FileVersion;

                //No logger yet initialized, so log to console
                Console.WriteLine("Initializing application");
                Console.WriteLine($"Version: {version}");

                totalStopWatch.Start();

                var serviceCollection = AppSetup.SetUp();

                serviceProvider = serviceCollection.BuildServiceProvider();

                Log.Logger.Information("Application initialized");
            }
            catch (Exception exception)
            {
                //Can not be sure logger is initialized, so log to console
                Console.WriteLine($"Error while initializing application: {exception}");

                Console.Read();
                return;
            }

            try
            {
                serviceProvider.GetService<ITvMazeScraper>().Scrape().Wait();

                totalStopWatch.Stop();
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error while running application: {exception.Message} \r\nDetailed exception is in the log.");
            }

            Console.Read();
        }
    }
}
