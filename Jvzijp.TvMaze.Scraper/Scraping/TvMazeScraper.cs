﻿using System.Linq;
using System.Threading.Tasks;
using JvZijp.TvMaze.Domain.Fetching;
using JvZijp.TvMaze.Domain.Storage;
using Serilog;

namespace JvZijp.TvMaze.Scraper.Scraping
{
    class TvMazeScraper : ITvMazeScraper
    {
        private readonly IShowsFetcher _showsFetcher;
        private readonly IShowStorage _showStorage;
        private readonly ICastFetcher _castFetcher;
        private readonly ICastStorage _castStorage;

        public TvMazeScraper(IShowsFetcher showsFetcher, IShowStorage showStorage, ICastFetcher castFetcher, ICastStorage castStorage)
        {
            _showsFetcher = showsFetcher;
            _showStorage = showStorage;
            _castFetcher = castFetcher;
            _castStorage = castStorage;
        }

        public async Task<ScrapeResult> Scrape()
        {
            Log.Information("Scraping...");

            var shows = await _showsFetcher.Fetch();

            Log.Information($"{shows.Count()} shows found");

            foreach (var show in shows)
            {
                _showStorage.Add(show);
            }

            foreach (var show in _showStorage.ShowsWithNoFetchedCast())
            {
                //ToDo: do this in a transaction
                var castMembers = await _castFetcher.FetchCastMembers(show);
                Log.Information($"{castMembers?.Count()} cast members found");

                if (castMembers != null)
                {
                    foreach (var castMember in castMembers)
                    {
                        if (!show.Cast.Any(member => member.Id == castMember.Id))
                            show.Cast.Add(castMember);

                        _castStorage.Add(castMember);
                    }
                }

                show.HasFetchedCast = true;
                _showStorage.Update(show);
                //ToDo: end transaction
            }

            return ScrapeResult.Success;
        }
    }
}
