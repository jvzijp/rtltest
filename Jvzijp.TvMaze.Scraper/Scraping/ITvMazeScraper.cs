﻿using System.Threading.Tasks;

namespace JvZijp.TvMaze.Scraper.Scraping
{
    interface ITvMazeScraper
    {
        Task<ScrapeResult> Scrape();
    }
}
