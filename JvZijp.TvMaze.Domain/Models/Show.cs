﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JvZijp.TvMaze.Domain.Models
{
    public class Show
    {
        public Show()
        {
            Cast = new List<CastMember>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] //ToDo: These database annotations should be in a DTO object in the storage project
        [JsonProperty("id")]  //ToDo: These api annotations should be in a api object in the Api project
        public int Id { get; set; }

        [JsonProperty("name")]  //ToDo: These api annotations should be in a api object in the Api project
        public string Name { get; set; }

        [JsonProperty("cast")]  //ToDo: These api annotations should be in a api object in the Api project
        public ICollection<CastMember> Cast { get; set;}

        [JsonIgnore]
        public bool HasFetchedCast { get; set; }
    }
}
