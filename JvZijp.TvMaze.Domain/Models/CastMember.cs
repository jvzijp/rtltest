﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace JvZijp.TvMaze.Domain.Models
{
    public class CastMember
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] //ToDo: These database annotations should be in a DTO object in the storage project
        [JsonProperty("id")]  //ToDo: These api annotations should be in a api object in the Api project
        public int Id { get; set; }

        [JsonProperty("name")]  //ToDo: These api annotations should be in a api object in the Api project
        public string Name { get; set; }

        [JsonProperty("birthday")]  //ToDo: These api annotations should be in a api object in the Api project
        public DateTime? BirthDate { get; set; }
    }
}
