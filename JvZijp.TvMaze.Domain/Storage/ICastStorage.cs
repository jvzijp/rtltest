﻿using System.Collections.Generic;
using JvZijp.TvMaze.Domain.Models;

namespace JvZijp.TvMaze.Domain.Storage
{
    public interface ICastStorage
    {
        StoreResult Add(CastMember castMember);
        IEnumerable<CastMember> Get(Show show);
    }
}
