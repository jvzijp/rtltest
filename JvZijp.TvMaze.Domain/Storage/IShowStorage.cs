﻿using System.Collections.Generic;
using JvZijp.TvMaze.Domain.Models;

namespace JvZijp.TvMaze.Domain.Storage
{
    public interface IShowStorage
    {
        StoreResult Add(Show show);
        StoreResult Update(Show show);
        IEnumerable<Show> ShowsWithNoFetchedCast();

        Show GetLatest();
        IEnumerable<Show> Get(int pageNumber);
    }
}
