﻿namespace JvZijp.TvMaze.Domain.Storage
{
    public enum StoreResult
    {
        Fail,
        Success,
        Skipped
    }
}
