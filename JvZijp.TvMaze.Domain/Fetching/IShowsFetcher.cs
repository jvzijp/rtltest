﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JvZijp.TvMaze.Domain.Models;

namespace JvZijp.TvMaze.Domain.Fetching
{
    public interface IShowsFetcher
    {
        Task<IEnumerable<Show>> Fetch();
    }
}
