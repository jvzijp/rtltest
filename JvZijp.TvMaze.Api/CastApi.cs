﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using JvZijp.TvMaze.Domain.Fetching;
using JvZijp.TvMaze.Domain.Models;
using Newtonsoft.Json;
using Serilog;

namespace JvZijp.TvMaze.Api
{
    class CastApi : ICastFetcher
    {
        public async Task<IEnumerable<CastMember>> FetchCastMembers(Show show)
        {
            var client = new HttpClient();

            try
            {
                var url = $"http://api.tvmaze.com/shows/{show.Id}/cast"; //ToDo: Make configurable
                Log.Information($"Getting data from {url}");

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    Log.Information("Fetch successful");

                    var responseString = await response.Content.ReadAsStringAsync();
                    var castMembers = JsonConvert.DeserializeObject<List<ApiCastMember>>(responseString);

                    return castMembers.Select(m => m.person);
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception occurred", e);
            }
            return null;
        }
    }
}
