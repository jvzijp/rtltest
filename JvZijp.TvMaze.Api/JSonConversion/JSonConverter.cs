﻿using System;
using System.Collections.Generic;
using JvZijp.TvMaze.Domain.Models;
using Newtonsoft.Json;

namespace JvZijp.TvMaze.Api.JSonConversion
{
    class JSonConverter : IJsonConverter
    {
        public List<Show> ConvertFromJsonString(string jsonString)
        {
            return JsonConvert.DeserializeObject<List<Show>>(jsonString);
        }
    }
}
