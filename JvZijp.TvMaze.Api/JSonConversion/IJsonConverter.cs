﻿using System;
using System.Collections.Generic;
using System.Text;
using JvZijp.TvMaze.Domain.Models;

namespace JvZijp.TvMaze.Api.JSonConversion
{
    public interface IJsonConverter
    {
        List<Show> ConvertFromJsonString(string jsonString);
    }
}
