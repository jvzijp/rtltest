﻿using JvZijp.TvMaze.Api.ClientWrapper;
using JvZijp.TvMaze.Api.JSonConversion;
using JvZijp.TvMaze.Domain.Fetching;
using Microsoft.Extensions.DependencyInjection;

namespace JvZijp.TvMaze.Api.Configuration
{
    public static class ProjectSetup
    {
        public static void ConfigureProject(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IShowsFetcher, ShowsApi>();
            serviceCollection.AddSingleton<ICastFetcher, CastApi>();
            serviceCollection.AddSingleton<IHttpClientWrapper, HttpClientWrapper>();
            serviceCollection.AddSingleton<IJsonConverter, JSonConverter>();
        }
    }
}
