﻿using System.Net.Http;
using System.Threading.Tasks;

namespace JvZijp.TvMaze.Api.ClientWrapper
{
    public interface IHttpClientWrapper
    {
        Task<HttpResponseMessage> GetAsync(string requestUri);
    }
}
