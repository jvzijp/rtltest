﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using JvZijp.TvMaze.Api.ClientWrapper;
using JvZijp.TvMaze.Api.JSonConversion;
using JvZijp.TvMaze.Domain.Fetching;
using JvZijp.TvMaze.Domain.Models;
using JvZijp.TvMaze.Domain.Storage;
using Newtonsoft.Json;
using Serilog;

namespace JvZijp.TvMaze.Api
{
    public class ShowsApi : IShowsFetcher
    {
        private readonly IShowStorage _showStorage;
        private readonly IHttpClientWrapper _httpClientWrapper;
        private readonly IJsonConverter _jsonConverter;

        public ShowsApi(IShowStorage showStorage, IHttpClientWrapper httpClientWrapper, IJsonConverter jsonConverter)
        {
            _showStorage = showStorage;
            _httpClientWrapper = httpClientWrapper;
            _jsonConverter = jsonConverter;
        }

        public async Task<IEnumerable<Show>> Fetch()
        {
            var shows = new List<Show>();
            var showsOnPage = new List<Show>();

            var latestShow = _showStorage.GetLatest();
            var pageNumber = latestShow?.Id / 250 ?? 0;
            bool retry;

            do
            {
                retry = false;

                var url = $"http://api.tvmaze.com/shows?page={pageNumber}"; //ToDo: Make configurable
                Log.Information($"Getting data from {url}");

                var response = await _httpClientWrapper.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    Log.Information("Fetch successful");

                    var responseString = await response.Content.ReadAsStringAsync();
                    showsOnPage = _jsonConverter.ConvertFromJsonString(responseString);

                    Log.Information($"{showsOnPage.Count} shows found on page");

                    shows.AddRange(showsOnPage);
                    Log.Information($"{shows.Count} shows found in total");

                    pageNumber++;
                }
                else if (response.StatusCode == HttpStatusCode.TooManyRequests)
                {
                    Log.Information("Too many requests, waiting 10 seconds");

                    Thread.Sleep(10000); //ToDo: Make configurable
                    retry = true;
                }
                else
                {
                    // Non-recoverable error
                    Log.Information($"Invalid StatusCode returned: {response.StatusCode}");
                    break;
                }

            } while (showsOnPage.Count > 0 || retry);

            return shows;
        }
    }
}
