using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using JvZijp.TvMaze.Api.ClientWrapper;
using JvZijp.TvMaze.Api.JSonConversion;
using JvZijp.TvMaze.Domain.Models;
using JvZijp.TvMaze.Domain.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace JvZijp.TvMaze.Api.Tests
{
    [TestClass]
    public class ShowsApiTests
    {
        [TestMethod]
        public void TestFetch_FirstPageNotFound_NoResults()
        {
            //Arrange

            var showStorageMock = new Mock<IShowStorage>(MockBehavior.Strict);
            showStorageMock.Setup(ss => ss.GetLatest())
                .Returns(new Show {Id = 1010});

            var httpClientWrapperMock = new Mock<IHttpClientWrapper>(MockBehavior.Strict);
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=4"))
                .Returns(Task.FromResult(new HttpResponseMessage{StatusCode = HttpStatusCode.NotFound}));

            var showsApi = new ShowsApi(showStorageMock.Object, httpClientWrapperMock.Object, null);

            //Act
            var result = showsApi.Fetch();

            //Assert
            Assert.AreEqual(TaskStatus.RanToCompletion, result.Status);
            Assert.AreEqual(0, result.Result.Count());
        }

        [TestMethod]
        public void TestFetch_FirstPageTooManyRequests_Retries()
        {
            //Arrange

            var showStorageMock = new Mock<IShowStorage>(MockBehavior.Strict);
            showStorageMock.Setup(ss => ss.GetLatest())
                .Returns(new Show { Id = 1010 });

            var httpClientWrapperMock = new Mock<IHttpClientWrapper>(MockBehavior.Strict);
            httpClientWrapperMock.SetupSequence(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=4"))
                .Returns(Task.FromResult(new HttpResponseMessage {StatusCode = HttpStatusCode.TooManyRequests}))
                .Returns(Task.FromResult(new HttpResponseMessage {StatusCode = HttpStatusCode.NotFound }));

            var showsApi = new ShowsApi(showStorageMock.Object, httpClientWrapperMock.Object, null);

            //Act
            var result = showsApi.Fetch();

            //Assert
            Assert.AreEqual(TaskStatus.RanToCompletion, result.Status);
            Assert.AreEqual(0, result.Result.Count());
            httpClientWrapperMock.Verify(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=4"), Times.Exactly(2));
        }

        [TestMethod]
        public void TestFetch_FirstPageOk_SecondNotFound_Results()
        {
            //Arrange

            var showStorageMock = new Mock<IShowStorage>(MockBehavior.Strict);
            showStorageMock.Setup(ss => ss.GetLatest())
                .Returns(new Show { Id = 1010 });

            var httpClientWrapperMock = new Mock<IHttpClientWrapper>(MockBehavior.Strict);
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=4"))
                .Returns(Task.FromResult(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("This is JSON")
                }));
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=5"))
                .Returns(Task.FromResult(new HttpResponseMessage { StatusCode = HttpStatusCode.NotFound }));

            var shows = Enumerable.Repeat(new Show(), 10).ToList();
            var jsonConverterMock = new Mock<IJsonConverter>(MockBehavior.Strict);
            jsonConverterMock.Setup(jc => jc.ConvertFromJsonString("This is JSON"))
                .Returns(shows);

            var showsApi = new ShowsApi(showStorageMock.Object, httpClientWrapperMock.Object, jsonConverterMock.Object);

            //Act
            var result = showsApi.Fetch();

            //Assert
            Assert.AreEqual(TaskStatus.RanToCompletion, result.Status);
            Assert.AreEqual(shows.Count, result.Result.Count());
        }

        [TestMethod]
        public void TestFetch_MultiplePagesWithResults_Results_AreConcatenated()
        {
            //Arrange

            var showStorageMock = new Mock<IShowStorage>(MockBehavior.Strict);
            showStorageMock.Setup(ss => ss.GetLatest())
                .Returns(new Show { Id = 1010 });

            var httpClientWrapperMock = new Mock<IHttpClientWrapper>(MockBehavior.Strict);
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=4"))
                .Returns(Task.FromResult(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("This is JSON for page 4")
                }));
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=5"))
                .Returns(Task.FromResult(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("This is JSON for page 5")
                }));
            httpClientWrapperMock.Setup(hcm => hcm.GetAsync("http://api.tvmaze.com/shows?page=6"))
                .Returns(Task.FromResult(new HttpResponseMessage { StatusCode = HttpStatusCode.NotFound }));

            var showsPage4 = Enumerable.Repeat(new Show(), 9).ToList();
            var showsPage5 = Enumerable.Repeat(new Show(), 12).ToList();
            var jsonConverterMock = new Mock<IJsonConverter>(MockBehavior.Strict);
            jsonConverterMock.Setup(jc => jc.ConvertFromJsonString("This is JSON for page 4"))
                .Returns(showsPage4);
            jsonConverterMock.Setup(jc => jc.ConvertFromJsonString("This is JSON for page 5"))
                .Returns(showsPage5);

            var showsApi = new ShowsApi(showStorageMock.Object, httpClientWrapperMock.Object, jsonConverterMock.Object);

            //Act
            var result = showsApi.Fetch();

            //Assert
            Assert.AreEqual(TaskStatus.RanToCompletion, result.Status);
            Assert.AreEqual(showsPage4.Count + showsPage5.Count, result.Result.Count());
        }

    }
}
