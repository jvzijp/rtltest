﻿using System;
using System.Collections.Generic;
using System.Linq;
using JvZijp.TvMaze.Domain.Models;
using JvZijp.TvMaze.Domain.Storage;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace JvZijp.TvMaze.Storage.SqlImpl
{
    class ShowStorage : IShowStorage
    {
        public StoreResult Add(Show show)
        {
            using (var context = new TvMazeContext())
            {
                Log.Information($"Adding show {show.Name}");

                if (context.Shows.Contains(show)) return StoreResult.Skipped;

                try
                {
                    context.Shows.Add(show);
                    context.SaveChanges();
                    return StoreResult.Success;
                }
                catch (Exception e)
                {
                    Log.Error($"Error adding show {show.Id}", e);
                    return StoreResult.Fail;
                }
            }
        }

        public StoreResult Update(Show show)
        {
            using (var context = new TvMazeContext())
            {
                Log.Information($"Updating show {show.Name}");

                if (!context.Shows.Contains(show)) return StoreResult.Skipped;

                try
                {
                    context.Shows.Update(show);
                    context.SaveChanges();
                    return StoreResult.Success;
                }
                catch (Exception e)
                {
                    Log.Error($"Error updating show {show.Id}", e);
                    return StoreResult.Fail;
                }
            }
        }

        public IEnumerable<Show> ShowsWithNoFetchedCast()
        {
            using (var context = new TvMazeContext())
            {
                return context.Shows.Where(show => !show.HasFetchedCast).ToList();
            }
        }

        public Show GetLatest()
        {
            using (var context = new TvMazeContext())
            {
                return context.Shows.OrderByDescending(show => show.Id).FirstOrDefault();
            }
        }

        public IEnumerable<Show> Get(int pageNumber)
        {
            const int pageSize = 20;

            using (var context = new TvMazeContext())
            {
                return context.Shows
                    .OrderBy(show => show.Id)
                    .Skip(pageNumber * pageSize).Take(pageSize)
                    .Include(show => show.Cast)
                    .ToList();
            }
        }
    }
}
