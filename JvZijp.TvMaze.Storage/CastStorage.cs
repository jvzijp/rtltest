﻿using System;
using System.Collections.Generic;
using System.Linq;
using JvZijp.TvMaze.Domain.Models;
using JvZijp.TvMaze.Domain.Storage;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace JvZijp.TvMaze.Storage.SqlImpl
{
    class CastStorage : ICastStorage
    {
        public StoreResult Add(CastMember castMember)
        {
            using (var context = new TvMazeContext())
            {
                Log.Information($"Storing cast {castMember.Name}");

                if (context.CastMembers.Contains(castMember)) return StoreResult.Skipped;

                try
                {
                    context.CastMembers.Add(castMember);
                    context.SaveChanges();
                    return StoreResult.Success;
                }
                catch (Exception e)
                {
                    Log.Error($"Error storing cast {castMember.Id}", e);
                    return StoreResult.Fail;
                }
            }
        }

        public IEnumerable<CastMember> Get(Show show)
        {

            using (var context = new TvMazeContext())
            {
                var showWithCast = context.Shows.Where(s => s.Id == show.Id)
                    .Include(s => s.Cast).First();

                return showWithCast.Cast.OrderBy(cast => cast.BirthDate);
            }

        }
    }
}
