﻿using JvZijp.TvMaze.Api;
using JvZijp.TvMaze.Domain.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace JvZijp.TvMaze.Storage.SqlImpl.Configuration
{
    public static class ProjectSetup
    {
        public static void ConfigureProject(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IShowStorage, ShowStorage>();
            serviceCollection.AddSingleton<ICastStorage, CastStorage>();
        }
    }
}
