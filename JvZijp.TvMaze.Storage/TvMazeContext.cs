﻿using JvZijp.TvMaze.Api;
using JvZijp.TvMaze.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace JvZijp.TvMaze.Storage.SqlImpl
{
    class TvMazeContext : DbContext
    {
        public DbSet<Show> Shows { get; set; }
        public DbSet<CastMember> CastMembers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=localhost;Database=TvMaze;Integrated Security=True"); //ToDo: Make ConnectionString configurable
        }
    }
}
