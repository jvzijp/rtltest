﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JvZijp.TvMaze.Domain.Models;
using JvZijp.TvMaze.Domain.Storage;
using Microsoft.AspNetCore.Mvc;

namespace JvZijp.TvMaze.RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
        private readonly IShowStorage _showStorage;

        public ShowsController(IShowStorage showStorage)
        {
            _showStorage = showStorage;
        }

        // GET api/shows/page/5
        [HttpGet("page/{pageId}")]
        public JsonResult GetPage(int pageId)
        {
            var showsOnPage = _showStorage.Get(pageId);

            var showsWithOrderedCast = showsOnPage.Select
                (show =>
                    new Show { Id = show.Id, Name = show.Name, Cast = show.Cast.OrderBy(cast => cast.BirthDate).ToList() });


            return new JsonResult(showsWithOrderedCast, new Newtonsoft.Json.JsonSerializerSettings { Formatting = Newtonsoft.Json.Formatting.Indented });
        }
    }
}
