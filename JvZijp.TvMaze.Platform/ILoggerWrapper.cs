﻿namespace JvZijp.TvMaze.Platform
{
    public interface ILoggerWrapper
    {
        void Debug(string msg);
        void Error(string msg);
    }
}
